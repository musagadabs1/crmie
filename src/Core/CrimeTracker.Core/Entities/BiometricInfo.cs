﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CrimeTracker.Core.Entities
{
    [Table("biometric")]
    [NotMapped]
    public class BiometricInfo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string template { get; set; }
        public int imageWidth { get; set; }
        public int imageHeight { get; set; }
        public int imageDPI { get; set; }
        public int imageQuality { get; set; }
        public string fingerPosition { get; set; }
        public string serialNumber { get; set; }
        public string model { get; set; }
        public string manufacturer { get; set; }
        public string created_by { get; set; }
        public DateTime date_created { get; set; }
        [ForeignKey("criminal_details")]
        public string criminalNo { get; set; }

    }
}
