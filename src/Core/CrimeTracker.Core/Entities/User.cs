﻿using System;

namespace CrimeTracker.Core.Entities
{
    public class User
    {
        public virtual int Id { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }

        public virtual string Email { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual int UserRoleId { get; set; }
        public virtual int FailedPasswordAttemptCount { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual DateTime LastLoginDate { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual UserRole UserRole { get; set; }
        public virtual ProfileStatus Status { get; set; }
    }
    public enum ProfileStatus
    {
        Disabled, Enabled
    }
}
