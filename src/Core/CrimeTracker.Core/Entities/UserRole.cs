﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrimeTracker.Core.Entities
{
    public class UserRole
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Functions { get; set; }
        public virtual string Description { get; set; }
    }
}
