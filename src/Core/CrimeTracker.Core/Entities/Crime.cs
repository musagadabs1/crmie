﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CrimeTracker.Core.Entities
{
    [Table("crime_details")]
    public class Crime:BaseEntity
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //public int Id { get; set; }
        [ForeignKey("criminal")]
        public string CriminalNo { get; set; }
        [Key]
        public string CrimeNo { get; set; }
        public string CrimeName { get; set; }
        public string CrimeClass { get; set; }
        public string PlaceOfArrest { get; set; }
        public string ArrestingOfficer { get; set; }
        public DateTime DateOfArrest { get; set; }
        public string Status { get; set; }
    }
}
