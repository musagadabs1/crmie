﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CrimeTracker.Core.Entities
{
    [Table("criminal_histories")]
    public class CriminalHistory:BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [ForeignKey("criminal_details")]
        public string criminal_no { get; set; }
        public DateTime date { get; set; }
        public string charge_and_disposition { get; set; }
        public string city { get; set; }
    }
}
