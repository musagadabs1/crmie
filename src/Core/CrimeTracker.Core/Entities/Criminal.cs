﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CrimeTracker.Core.Entities
{
    [Table("criminal_details")]
    public class Criminal:BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [Key]
        public string criminal_no { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }
        public string sex { get; set; }
        public int age { get; set; }
        public DateTime date_of_birth { get; set; }
        public string height { get; set; }
        public string weight { get; set; }
        public string eye_color { get; set; }
        public string hair_color { get; set; }
        public string build { get; set; }
        public string scars_and_marks { get; set; }
        public string occupation { get; set; }
        public string known_gang_affiliation { get; set; }
        public string citizen { get; set; }
        public string national_identity { get; set; }
        public string image_path { get; set; }
    }
}
