﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CrimeTracker.Core.Entities
{
    [Table("criminal_address")]
    public class CriminalAddress:BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [ForeignKey("criminal_details")]
        public string criminal_no { get; set; }
        public string residence { get; set; }
        public string phone_no { get; set; }
        public string email { get; set; }
        public string state { get; set; }
        public string lgc { get; set; }
        public string city { get; set; }
        public string Status { get; set; }
    }
}
