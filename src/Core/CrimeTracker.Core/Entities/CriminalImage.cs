﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CrimeTracker.Core.Entities
{
    [Table("criminal_images")]
    public class CriminalImage:BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        [ForeignKey("criminal_details")]
        public string criminal_no { get; set; }
        public string imag_path { get; set; }
    }
}
