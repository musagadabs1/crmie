﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace CrimeTracker.Core.Utility
{
    public class Utils
    {
        public static string PasCaseConversion(string PascalWord)
        {
            return Regex.Replace(PascalWord, "[a-z][A-Z]", m => $"{m.Value[0]} {char.ToLower(m.Value[1])}");
        }

        public static string GetMd5Hash(string value)
        {
            var md5Hasher = MD5.Create();
            var data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            var sBuilder = new StringBuilder();
            for (var i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        //public static string GetAppConfigItem(string itemKey)
        //{
        //    var builder = new ConfigurationBuilder()
        //        .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
        //        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
        //    var configuration = builder.Build();
        //    if (configuration.GetSection(itemKey) != null)
        //    {
        //        return configuration.GetSection(itemKey).Value;
        //    }
        //    return null;
        //}
        //public static string GetConnectionString(string itemKey)
        //{
        //    var builder = new ConfigurationBuilder()
        //         .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
        //         .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
        //    string connectionString = builder.Build().GetConnectionString(itemKey);//"myDB"
        //    return connectionString;
        //}

        //public static List<T> ConvertToList<T>(DataTable dt)
        //{
        //    var columnNames = dt.Columns.Cast<DataColumn>()
        //            .Select(c => c.ColumnName).ToList();

        //    var properties = typeof(T).GetProperties();
        //    return dt.AsEnumerable().Select(row =>
        //    {
        //        var objT = Activator.CreateInstance<T>();
        //        foreach (var pro in properties)
        //        {
        //            if (columnNames.Contains(pro.Name))
        //            {
        //                PropertyInfo pI = objT.GetType().GetProperty(pro.Name);
        //                pro.SetValue(objT, row[pro.Name] == DBNull.Value ? null : Convert.ChangeType(row[pro.Name], pI.PropertyType));
        //            }
        //        }

        //        return objT;
        //    }).ToList();
        //}
    }
}
