﻿using FingerPrintModule.DAO;
using FingerPrintModule.Facade;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace FingerPrintModule.Controllers
{
    public class FingerPrintController : ApiController
    {

        [HttpGet]
        public FingerPrintInfo CapturePrint(int fingerPosition)
        {
            var fingerPrintFacade = new FingerPrintFacade();
            var data = fingerPrintFacade.Capture(fingerPosition, out string err, false);

            if (string.IsNullOrEmpty(err))
            {
                var db = new DataAccess();
                var previously = db.GetBiometricinfo();

                var matchedCriminalId = fingerPrintFacade.Verify(new FingerPrintMatchInputModel
                {
                    FingerPrintTemplate = data.Template,
                    FingerPrintTemplateListToMatch = new List<FingerPrintInfo>(previously)
                });

                if (matchedCriminalId != "")
                {
                    var info = db.RetrieveCriminalIdIdByID(matchedCriminalId);
                    var name = info.Split('|')[0];
                    var UniqueId = info.Split('|')[1];
                    data.ErrorMessage = string.Format($"Finger print record already exist for this person {Environment.NewLine} Name : {name} {Environment.NewLine}  Identifier : {UniqueId}");
                }
            }
            else
            {
                data = new FingerPrintInfo();
                data.ErrorMessage = err;
            }
            return data;
        }

        [HttpGet]
        public List<FingerPrintInfo> CheckForPreviousCapture(string criminalNo)
        {
            if (ConfirmDBSetting())
            {
                var db = new DataAccess();
                var criminalInfo = db.RetrieveCriminalIdIdByID(criminalNo);
                var criminalInfoSplit = criminalInfo.Split('|');
                string sid = criminalInfoSplit[1];
                if (criminalInfo != null && ( sid != null || sid !=""))
                {
                    var previously = db.GetBiometricinfo(sid);
                    return previously;
                }
                else
                {
                    return null;
                }
            }
            else
                throw new ApplicationException("Invalid database connection ");
        }


        [HttpPost]
        public string MatchFingerPrint(FingerPrintMatchInputModel input)
        {

            var fingerPrintFacade = new FingerPrintFacade();
            var matchedCriminalId = fingerPrintFacade.Verify(input);
            
            return JsonConvert.SerializeObject(new
            {
                PatientId = matchedCriminalId,
                Matched = matchedCriminalId != ""
            });
        }


        [HttpPost]
        public ResponseModel SaveToDatabase(SaveModel model)
        {
            var db = new DataAccess();
            var criminalNo = model.criminalNo;
            var criminalInfo = db.RetrieveCriminalIdIdByID(criminalNo);

            if (criminalInfo != null && !string.IsNullOrEmpty(criminalInfo.Split('|')[1]))
            {
                model.FingerPrintList.ForEach(x =>
                    {
                        x.criminalNo = criminalInfo.Split('|')[1];
                    }
                );
                return db.SaveToDatabase(model.FingerPrintList);
            }
            else
            {
                return new ResponseModel
                {
                    ErrorMessage = "Invalid Criminal No supplied",
                    IsSuccessful = false
                };
            }
        }


        public bool ConfirmDBSetting()
        {
            var db = new DataAccess(DataAccess.GetConnectionString());
            //check if connection is valid
            //if connection string is wrong an error would occur here and will return error
            db.ExecuteScalar(string.Format("Select DB_ID('crime_tracker_db');"));

            db.ExecuteQuery("CheckBiometricTable");
            return true;
        }
    }
}
