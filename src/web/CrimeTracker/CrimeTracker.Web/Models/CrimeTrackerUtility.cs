﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CrimeTracker.Web.Models
{
    public class CrimeTrackerUtility
    {
        public static string CriminalNo { get; set; }

        public static void SendEmail(string email, string message, string subject, string smtpServer="", string smtpLogin="",string smtpPassword="",bool ssl_enabled=false,int port=0,float email_timeout=0.0f,string from="")
        {

            try
            {
                //var smtpServer = appSetting.Value.SmtpServer;
                //var smtpLogin = appSetting.Value.SmtpLogin;
                //var smtpPassword = appSetting.Value.smtpPassword;
                //var ssl_enabled = appSetting.Value.SslEnable;
                //var port = appSetting.Value.Port;
                //var email_timeout = appSetting.Value.Timeout;
                //var from = appSetting.Value.From;

                // Credentials
                var credentials = new NetworkCredential(smtpLogin, smtpPassword);
                // Mail message
                var mail = new MailMessage()
                {
                    From = new MailAddress(from),
                    Subject = subject,
                    Body = message
                };
                mail.IsBodyHtml = true;
                mail.To.Add(new MailAddress(email));
                // Smtp client
                var client = new SmtpClient()
                {
                    Port = port,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Host = smtpServer,
                    EnableSsl = ssl_enabled,
                    Credentials = credentials
                };
                client.Send(mail);
                //return "Email Sent Successfully!";
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
    public class EmailReputationService
    {
        public HttpClient Client { get; }
        public EmailReputationService(HttpClient client)
        {
            client.BaseAddress = new Uri("https://emailrep.io/");
            Client = client;
        }
        public async Task<ReputationResponse> GetEmailReputation(string emialAddress)
        {
            var response = await Client.GetAsync(emialAddress);
            response.EnsureSuccessStatusCode();
            using var responseStream = await response.Content.ReadAsStreamAsync();
            
            return await JsonSerializer.DeserializeAsync<ReputationResponse>(responseStream);
        }
    }
    public class ReputationResponse
    {
        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("reputation")]
        public string Reputation { get; set; }

        [JsonPropertyName("suspicious")]
        public bool Suspicious { get; set; }

        [JsonPropertyName("references")]
        public long References { get; set; }

        [JsonPropertyName("details")]
        public ReputationDetails Details { get; set; }

        public class ReputationDetails
        {
            [JsonPropertyName("blacklisted")]
            public bool Blacklisted { get; set; }

            [JsonPropertyName("malicious_activity")]
            public bool MaliciousActivity { get; set; }

            [JsonPropertyName("malicious_activity_recent")]
            public bool MaliciousActivityRecent { get; set; }

            [JsonPropertyName("credentials_leaked")]
            public bool CredentialsLeaked { get; set; }

            [JsonPropertyName("credentials_leaked_recent")]
            public bool CredentialsLeakedRecent { get; set; }

            [JsonPropertyName("data_breach")]
            public bool DataBreach { get; set; }

            [JsonPropertyName("first_seen")]
            public string FirstSeen { get; set; }

            [JsonPropertyName("last_seen")]
            public string LastSeen { get; set; }

            [JsonPropertyName("domain_exists")]
            public bool DomainExists { get; set; }

            [JsonPropertyName("domain_reputation")]
            public string DomainReputation { get; set; }

            [JsonPropertyName("new_domain")]
            public bool NewDomain { get; set; }

            [JsonPropertyName("days_since_domain_creation")]
            public long DaysSinceDomainCreation { get; set; }

            [JsonPropertyName("suspicious_tld")]
            public bool SuspiciousTld { get; set; }

            [JsonPropertyName("spam")]
            public bool Spam { get; set; }

            [JsonPropertyName("free_provider")]
            public bool FreeProvider { get; set; }

            [JsonPropertyName("disposable")]
            public bool Disposable { get; set; }

            [JsonPropertyName("deliverable")]
            public bool Deliverable { get; set; }

            [JsonPropertyName("accept_all")]
            public bool AcceptAll { get; set; }

            [JsonPropertyName("valid_mx")]
            public bool ValidMx { get; set; }

            [JsonPropertyName("spoofable")]
            public bool Spoofable { get; set; }

            [JsonPropertyName("spf_strict")]
            public bool SpfStrict { get; set; }

            [JsonPropertyName("dmarc_enforced")]
            public bool DmarcEnforced { get; set; }

            [JsonPropertyName("profiles")]
            public string[] Profiles { get; set; }
        }
    }
}
