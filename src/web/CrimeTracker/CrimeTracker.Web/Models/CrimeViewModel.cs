﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrimeTracker.Web.Models
{
    public class CrimeViewModel
    {
        public string CrimeNo { get; set; }
        public string CrimeName { get; set; }
        public string CrimeClass { get; set; }
        public string PlaceOfArrest { get; set; }
        public string ArrestingOfficer { get; set; }
        public DateTime DateOfArrest { get; set; }
    }
}
