﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace CrimeTracker.Web.Models
{
    public class UserProfile : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }

        public string EmployeeId { get; set; }
        public string Rolename { get; set; }

        public DateTime CreationDate { get; set; }
        public int FailedPasswordAttemptCount { get; set; }
        public DateTime LastLoginDate { get; set; }
        public string Status { get; set; }
        public string ConfirmPassword { get; set; }

        public string Fullname => $"{FirstName} {LastName}";

        //public bool IsPasswordReset { get; set; }

        public UserRole UserRole { get; set; }
        public string TheRoleName => UserRole != null ? UserRole.Name : "";
        public Guid ProfileId { get; set; }
    }
    public class UserRole : IdentityRole
    {
        public virtual string Functions { get; set; }
        public virtual string Description { get; set; }
    }
    public class LoginViewModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
    public class ChangePasswordModel
    {
        [Required]
        public string UserId { get; set; }

        public string OldPassword { get; set; }

        [Required]
        public string NewPassword { get; set; }

        [Required]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public string Code { get; set; }
    }

    public class RegisterViewModel
    {
        //public virtual string Username { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public virtual string Password { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public virtual string ConfirmPassword { get; set; }

        [Required]
        public virtual string FirstName { get; set; }

        [Required]
        public virtual string LastName { get; set; }

        //[Phone]
        public virtual string PhoneNumber { get; set; }

        [Required]
        [EmailAddress]
        public virtual string Email { get; set; }

        [Required]
        public virtual string RoleName { get; set; }

        [Required]
        public string CreatedBy { get; set; }
        //[XmlIgnore]
        //[Required]
        //public Dictionary<string, string> Roles { get; set; }
    }
}
