﻿using CrimeTracker.Core.Entities;
using CrimeTracker.Web.APIServices;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CrimeTracker.Web.Services
{
    public class WebUtility
    {
        public static async Task<Criminal> GetEmployeeBio(HttpContext context, string criminalNo)
        {
            var criminalString = context.Session.GetString(":.criminal.:");
            Criminal criminal;
            if (!string.IsNullOrEmpty(criminalString))
            {
                criminal = Newtonsoft.Json.JsonConvert.DeserializeObject<Criminal>(criminalString);
                return criminal;
            }
            else
            {
                criminal = await new InternalAPI(URL.CriminalURL).GetMessage<Criminal>("/Criminals/GetSearchCriminal?criminalNo=" + criminalNo);
                context.Session.SetString(":.criminal.:", Newtonsoft.Json.JsonConvert.SerializeObject(criminal));
            }
            return criminal;
        }

        public static string GetAppConfigItem(string itemKey)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = builder.Build();
            if (configuration.GetSection(itemKey) != null)
            {
                return configuration.GetSection(itemKey).Value;
            }
            return null;
        }

        public static string DisabledModule()
        => GetAppConfigItem("DisabledModule");


        public static string GetConnectionString(string itemKey)
        {
            var builder = new ConfigurationBuilder()
                 .SetBasePath(Directory.GetCurrentDirectory())
                 .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var connectionString = builder.Build().GetConnectionString(itemKey);//"myDB"
            return connectionString;
        }
        public static string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }
        private static Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
    }
}
