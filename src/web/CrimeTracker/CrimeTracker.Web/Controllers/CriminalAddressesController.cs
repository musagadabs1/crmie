﻿using CrimeTracker.Core.Entities;
using CrimeTracker.Web.APIServices;
using CrimeTracker.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrimeTracker.Web.Controllers
{
    [Authorize]
    public class CriminalAddressesController : Controller
    {
        //private readonly CrimeTrackerDbContext _context;
        InternalAPI _criminalAddressApi;

        public CriminalAddressesController()
        {
            _criminalAddressApi=  new InternalAPI(URL.CriminalURL);
        }

        // GET: CriminalAddresses
        public async Task<IActionResult> Index()
        {
            string message;
            try
            {
                var urlPart = "CriminalAddresses/GetCriminalAddresses";

                var responseMgs = await _criminalAddressApi.GetMessage<List<CriminalAddress>>(urlPart);
                return View(responseMgs);
            }
            catch (Exception ex)
            {

                message = "Something wrong happened";
                ViewBag.Message = message;
                throw ex;
            }
        }
        public async Task<IActionResult> GetAddresses(string criminalNo)
        {
            string message;
            try
            {
                var urlPart = "CriminalAddresses/GetCriminalAddresses?criminal_no=" + criminalNo;

                var responseMgs = await _criminalAddressApi.GetMessage<List<CriminalAddress>>(urlPart);

                var criminalRecords = responseMgs;
                return View(responseMgs);
            }
            catch (Exception ex)
            {

                message = "Something wrong happened. "+ex.Message;
                ViewBag.Message = message;
                //throw ex;
                return View();
            }
        }

        //FilteredItems for Datatable server-side processing
        [HttpPost]
        public async Task<JsonResult> GetFilteredItems()
        {
            string errorMessage = string.Empty;
            try
            {
                //var urlPart = "CriminalAddresses/GetCriminalAddresses?criminalNo=" + CrimeTrackerUtility.CriminalNo;
                var criminalNo = "CRM001";
                var urlPart = "CriminalAddresses/GetCriminalAddresses?criminal_no=" + criminalNo;

                var responseMgs = await _criminalAddressApi.GetMessage<List<CriminalAddress>>(urlPart);

                var criminalRecords = responseMgs;

                System.Threading.Thread.Sleep(2000);
                int draw = Convert.ToInt32(Request.Query["draw"]);
                if (draw < 0)
                {
                    draw = 0;
                }
                // Data to be skipped
                // if 0 first "length" records will be fetched
                //if 1 second "length" of records will be fetched.
                int start = Convert.ToInt32(Request.Query["start"]);
                if (start < 0)
                {
                    start = 0;
                }
                // records count to be fetched after skip
                int length = Convert.ToInt32(Request.Query["length"]);
                if (length < 0)
                {
                    length = 0;
                }
                //Getting sort column name
                int sortColumnIdx = Convert.ToInt32(Request.Query["order[0][column]"]);
                string sortColumnName = Request.Query["columns[" + sortColumnIdx + "][name]"];

                //Sort column direction
                string sortColumnDirection = Request.Query["order[0][dir]"];

                // search value
                string searchValue = Request.Query["search[value]"].FirstOrDefault()?.Trim();

                //Record count matching search criteria
                int recordsFilteredCount = 0;
                if (searchValue != null)
                {
                    recordsFilteredCount = criminalRecords.Where(c => c.residence.Contains(searchValue)).Count();
                }
                //Total Record count
                int recordsTotalCount = criminalRecords.Count();

                //Filtered, sorted and paged data to be sent from server to view
                List<CriminalAddress> filteredData = null;
                if (sortColumnDirection == "asc")
                {
                    if (searchValue != null)
                    {
                        filteredData = criminalRecords.Where(c => c.residence.Contains(searchValue))
                                                .OrderBy(c => c.GetType().GetProperty(sortColumnName).GetValue(c))
                                                .Skip(start)
                                                .Take(length)
                                                .ToList();
                    }

                }
                else
                {
                    if (searchValue != null)
                    {
                        filteredData = criminalRecords.Where(c => c.residence.Contains(searchValue))
                                                .OrderByDescending(c => c.GetType().GetProperty(sortColumnName).GetValue(c))
                                                .Skip(start)
                                                .Take(length)
                                                .ToList();
                    }

                }
                if (filteredData == null)
                {
                    filteredData = criminalRecords;
                }

                //send data
                return Json(
                    new
                    {
                        data = filteredData,
                        draw = draw,
                        recordsFiltered = recordsFilteredCount,
                        recordsTotal = recordsTotalCount
                    }
                    );
            }
            catch (Exception ex)
            {
                errorMessage = "Something wrong has happened.";
                ViewBag.Message = errorMessage;
                throw ex;
            }


        }

        // GET: CriminalAddresses/Details/5
        public ActionResult Details(int? id)
        {
            //if (id == null)
            //{
            //    return NotFound();
            //}

            //var criminalAddress = await _context.CriminalAddresses
            //    .FirstOrDefaultAsync(m => m.id == id);
            //if (criminalAddress == null)
            //{
            //    return NotFound();
            //}

            //return View(criminalAddress);
            return View(id);
        }

        //GET: CriminalAddresses/Create
        public IActionResult SaveCriminalAddress()
        {
            return View();
        }

        // POST: CriminalAddresses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveCriminalAddress([FromBody] CriminalAddress criminalAddress)
        {
            var message = "";
            var successMessage = "Saved Successfully";

            try
            {
                criminalAddress.CreatedBy = User.Identity.Name;
                criminalAddress.CreatedOn = DateTime.Today;
                criminalAddress.criminal_no = CrimeTrackerUtility.CriminalNo;
                //criminalAddress.criminal_no = "CRM005";// CrimeTrackerUtility.CriminalNo;

                var urlPart = "CriminalAddresses/PostCriminalAddress";
                var responseMsg = await _criminalAddressApi.PostMessage(urlPart, criminalAddress);
                var responses = responseMsg.Split('|');
                var responseMessage = responses[0].ToLower();
                var criminalNo = responses[1];

                if (responseMessage == successMessage.ToLower())
                {
                    message = successMessage;
                    return Ok(responseMessage);
                }
                return BadRequest("Something bad happened. Check and try again.");
                
            }
            catch (Exception ex)
            {
                message = "Opp Something is wrong. " + ex.Message;
                ViewBag.Message = message;
                return BadRequest("Opp Something is wrong. " + ex.Message);
            }
        }

        // GET: CriminalAddresses/Edit/5
        public ActionResult Edit(int? id)
        {
            //if (id == null)
            //{
            //    return NotFound();
            //}

            //var criminalAddress = await _context.CriminalAddresses.FindAsync(id);
            //if (criminalAddress == null)
            //{
            //    return NotFound();
            //}
            //return View(criminalAddress);
            return View(id);
        }

        // POST: CriminalAddresses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id,  CriminalAddress criminalAddress)
        {
            if (id != criminalAddress.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                //try
                //{
                //    _context.Update(criminalAddress);
                //    await _context.SaveChangesAsync();
                //}
                //catch (DbUpdateConcurrencyException)
                //{
                //    if (!CriminalAddressExists(criminalAddress.id))
                //    {
                //        return NotFound();
                //    }
                //    else
                //    {
                //        throw;
                //    }
                //}
                //return RedirectToAction(nameof(Index));
            }
            return View(criminalAddress);
        }

        // GET: CriminalAddresses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var criminalAddress = await _context.CriminalAddresses
            //    .FirstOrDefaultAsync(m => m.id == id);
            //if (criminalAddress == null)
            //{
            //    return NotFound();
            //}

            //return View(criminalAddress);
            return View(id);
        }

        // POST: CriminalAddresses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //var criminalAddress = await _context.CriminalAddresses.FindAsync(id);
            //_context.CriminalAddresses.Remove(criminalAddress);
            //await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CriminalAddressExists(int id)
        {
            return false; //_context.CriminalAddresses.Any(e => e.id == id);
        }
    }
}
