﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrimeTracker.Web.APIServices;
using CrimeTracker.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CrimeTracker.Web.Controllers
{
    public class AccountController : Controller
    {
        readonly InternalAPI usersContext;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            usersContext = new InternalAPI(URL.CriminalURL);
            _userManager = userManager;
            _signInManager = signInManager;
        }
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        // GET: Account/Login
        public ActionResult Login()
        {
            return View();
        }

        // POST: Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            try
            {
                ViewData["ReturnUrl"] = returnUrl;
                if (ModelState.IsValid)
                {
                    var user = await usersContext.PostMessageWithData<IdentityUser, LoginViewModel>("Users/authenticate", model);
                    //HttpContext.Session.SetObject(":.User.:", user);
                    if (user != null)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);

                        if (string.IsNullOrEmpty(returnUrl))
                        {
                            return RedirectToAction("index", "home");
                        }
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                        return View(model);
                    }
                }
            }
            catch (Exception ex)
            {
                var errors = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(ex.Message);
                if (errors != null)
                {
                    foreach (var e in errors)
                    {
                        ModelState.AddModelError(e.Key, e.Value);
                    }
                }

                //var errors = ex.Message.Replace("{", " ").Replace("}", " ").Split(':');               
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: Account/Register
        public ActionResult Register()
        {
            return View();
        }
        // GET: Account/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Account/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Account/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Account/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Account/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Account/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Account/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}