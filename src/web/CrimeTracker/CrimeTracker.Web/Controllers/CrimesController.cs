﻿using CrimeTracker.Core.Entities;
using CrimeTracker.Web.APIServices;
using CrimeTracker.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrimeTracker.Web.Controllers
{
    [Authorize]
    public class CrimesController : Controller
    {
        //private readonly CrimeTrackerDbContext _context;
        InternalAPI _crimeApi;
        //InternalAPI _criminalApi;
        //InternalAPI
        public CrimesController()
        {
            _crimeApi = new InternalAPI(URL.CriminalURL);
        }

        

        // GET: Crimes
        public async Task<IActionResult> Index()
        {
            string message;
            try
            {
                var urlPart = "Crimes/GetCrimes";
                
                var responseMgs = await _crimeApi.GetMessage<List<Crime>>(urlPart);
                return View(responseMgs);
            }
            catch (Exception ex)
            {

                message = "Something wrong happened";
                ViewBag.Message = message;
                throw ex;
            }
        }
        // GET: Crimes/GetCrimes?criminalNo
        public async Task<IActionResult> GetCrimes(string criminalNo)
        {
            string message;
            var user = User.Identity.Name;
            try
            {
                var urlPart = "Crimes/GetCrimes?criminalNo=" + criminalNo;

                var responseMgs = await _crimeApi.GetMessage<List<Crime>>(urlPart);
                var criminalRecords = responseMgs;


                //var urlPart = "Crimes/GetCrimes";

                //var responseMgs = await _crimeApi.GetMessage<List<Crime>>(urlPart);
                return View(responseMgs);
            }
            catch (Exception ex)
            {

                message = "Something wrong happened. " + ex.Message;
                ViewBag.Message = message;
                //throw ex;
                return View();
            }
        }

        // GET: Crimes/Details/5
        public async Task<IActionResult> Details(string criminalId)
        {


            if (criminalId == null)
            {
                return NotFound();
            }

            string message;
            try
            {
                //Retrieve criminal record based on Criminal Number
                var criminalUrlPart = "Criminals/GetCriminal?id=" + criminalId;

                var criminalResponseMgs = await _crimeApi.GetMessage<Criminal>(criminalUrlPart);
                if (criminalResponseMgs==null)
                {
                    return NotFound();
                }
                //return View(responseMgs);
                ViewBag.Criminals = criminalResponseMgs;

                //Retrieve crime record based on Criminal Number
                var crimeUrlPart = "Crimes/GetCrimes?criminalNo=" + criminalResponseMgs.criminal_no;

                var crimeResponseMgs = await _crimeApi.GetMessage<List<Crime>>(crimeUrlPart);
                //return View(responseMgs);
                ViewBag.Crimes = crimeResponseMgs;

                //Retrieve criminal Address record based on Criminal Number
                var criminalAddressUrlPart = "CriminalAddresses/GetCriminalAddresses?criminal_no=" + criminalResponseMgs.criminal_no;

                var criminalAddressResponseMgs = await _crimeApi.GetMessage<List<CriminalAddress>>(criminalAddressUrlPart);
                //return View(responseMgs);
                ViewBag.Crimes = criminalAddressResponseMgs;


            }
            catch (Exception ex)
            {

                message = "Something wrong happened";
                ViewBag.Message = message;
                throw ex;
            }

            return View();
        }

        // GET: Crimes/Create
        public IActionResult SaveCrime()
        {
            return View();
        }

        // POST: Crimes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveCrime([FromBody] Crime crime)
        {
            var message = "";
            var successMessage = "Saved Successfully";

            try
            {
                crime.CreatedBy = User.Identity.Name;
                crime.CreatedOn = DateTime.Today;
                crime.CriminalNo = CrimeTrackerUtility.CriminalNo;
                //crime.CriminalNo = "CRM007";

                var urlPart = "Crimes/PostCrime";
                var responseMsg = await _crimeApi.PostMessage(urlPart, crime);
                var responses = responseMsg.Split('|');
                var responseMessage = responses[0].ToLower();
                //var criminalNo = responses[1];

                if (responseMessage == successMessage.ToLower())
                {
                    message = successMessage;
                    //ViewBag.Message = message;
                    return Ok(responseMessage);//RedirectToAction(nameof(Index));
                }
                return BadRequest("Something bad happened. Check and try again.");
                
            }
            catch (Exception ex)
            {
                message = "Opp Something is wrong. " + ex.Message;
                ViewBag.Message = message;
                return BadRequest("Opp Something is wrong. " + ex.Message);
            }
        }

        //FilteredItems for Datatable server-side processing
        [HttpPost]
        public async Task<JsonResult> GetFilteredItems(string criminalNo)
        {
            string errorMessage = string.Empty;
            try
            {
            //var urlPart = "Crimes/GetCrimes?criminalNo=" + CrimeTrackerUtility.CriminalNo;
            //var criminalRecords = new List<CrimeViewModel>();
            //http://localhost:30001/api/Crimes/GetCrimes?criminalNo=CRM001
                var urlPart = "Crimes/GetCrimes?criminalNo=" + criminalNo;

                var responseMgs = await _crimeApi.GetMessage<List<Crime>>(urlPart);
                var criminalRecords  = responseMgs;
                System.Threading.Thread.Sleep(2000);
                int draw = Convert.ToInt32(Request.Query["draw"]);
                if (draw < 0)
                {
                    draw = 0;
                }
                // Data to be skipped
                // if 0 first "length" records will be fetched
                //if 1 second "length" of records will be fetched.
                int start = Convert.ToInt32(Request.Query["start"]);
                if (start < 0)
                {
                    start = 0;
                }
                // records count to be fetched after skip
                int length = Convert.ToInt32(Request.Query["length"]);
                if (length < 0)
                {
                    length = 0;
                }
                //Getting sort column name
                int sortColumnIdx = Convert.ToInt32(Request.Query["order[0][column]"]);
                string sortColumnName = Request.Query["columns[" + sortColumnIdx + "][name]"];

                //Sort column direction
                string sortColumnDirection = Request.Query["order[0][dir]"];

                // search value
                string searchValue = Request.Query["search[value]"].FirstOrDefault()?.Trim();

                //Record count matching search criteria
                int recordsFilteredCount = 0;
                if (searchValue != null)
                {
                    recordsFilteredCount = criminalRecords.Where(c => c.CrimeName.Contains(searchValue)).Count();
                }
                //Total Record count
                int recordsTotalCount = criminalRecords.Count();

                //Filtered, sorted and paged data to be sent from server to view
                List<Crime> filteredData = null;
                if (sortColumnDirection == "asc")
                {
                    if (searchValue != null)
                    {
                        filteredData = criminalRecords.Where(c => c.CrimeName.Contains(searchValue))
                                                .OrderBy(c => c.GetType().GetProperty(sortColumnName).GetValue(c))
                                                .Skip(start)
                                                .Take(length)
                                                .ToList();
                    }

                }
                else
                {
                    if (searchValue != null)
                    {
                        filteredData = criminalRecords.Where(c => c.CrimeName.Contains(searchValue))
                                                .OrderByDescending(c => c.GetType().GetProperty(sortColumnName).GetValue(c))
                                                .Skip(start)
                                                .Take(length)
                                                .ToList();
                    }

                }
                if (filteredData == null)
                {
                    filteredData = criminalRecords;
                }

                //send data
                return Json(
                    new
                    {
                        data = filteredData,
                        draw = draw,
                        recordsFiltered = recordsFilteredCount,
                        recordsTotal = recordsTotalCount
                    }
                    );
            }
            catch (Exception ex)
            {
                errorMessage = "Something wrong has happened.";
                ViewBag.Message = errorMessage;
                throw ex;
            }


        }

        // GET: Crimes/Edit/5
        public async Task<ActionResult> Edit(string crimeNo)
        {
            if (crimeNo == null)
            {
                return NotFound();
            }
            var urlPart = "Crimes/GetCrimes?criminalNo=" + crimeNo;
            var responseMgs = await _crimeApi.GetMessage<Crime>(urlPart);
            //var criminalRecords = responseMgs;
            //var crime = await _crimeApi.GetMessage _context.Crimes.FindAsync(id);
            if (responseMgs == null)
            {
                return NotFound();
            }
            return View();
        }

        // POST: Crimes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(Crime crime)
        {
            //if (id != crime.CrimeNo)
            //{
            //    return NotFound();
            //}

            //if (ModelState.IsValid)
            //{
            //    try
            //    {
            //        _context.Update(crime);
            //        await _context.SaveChangesAsync();
            //    }
            //    catch (DbUpdateConcurrencyException)
            //    {
            //        if (!CrimeExists(crime.crime_no))
            //        {
            //            return NotFound();
            //        }
            //        else
            //        {
            //            throw;
            //        }
            //    }
            //    return RedirectToAction(nameof(Index));
            //}
            return View(crime);
        }

        // GET: Crimes/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var crime = await _context.Crimes
            //    .FirstOrDefaultAsync(m => m.crime_no == id);
            //if (crime == null)
            //{
            //    return NotFound();
            //}

            return View();
        }

        // POST: Crimes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            //var crime = await _context.Crimes.FindAsync(id);
            //_context.Crimes.Remove(crime);
            //await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        //private bool CrimeExists(string id)
        //{
        //    return false;// _context.Crimes.Any(e => e.crime_no == id);
        //}
    }
}
