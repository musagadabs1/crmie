﻿using CrimeTracker.Core.Entities;
using CrimeTracker.Web.APIServices;
using CrimeTracker.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CrimeTracker.Web.Controllers
{
    /// <summary>
    /// Criminal Controller
    /// </summary>
    [Authorize]
    public class CriminalsController : Controller
    {
        InternalAPI _criminalApi;
        /// <summary>
        /// Criminal Controller's constructor
        /// </summary>
        public CriminalsController()
        {
            _criminalApi = new InternalAPI(URL.CriminalURL);
        }
        // GET: Criminals
        public async Task<IActionResult> Index()
        {
            string message;
            try
            {
                var urlPart = "Criminals/GetCriminals";

                var responseMgs = await _criminalApi.GetMessage<List<Criminal>>(urlPart);
                return View(responseMgs);
            }
            catch (Exception)
            {

                message = "Something wrong happened";
                ViewBag.Message = message;
                return View();
            }
           
            
        }

        //FilteredItems for Datatable server-side processing
        [HttpPost]
        public async Task<JsonResult> GetFilteredItems()
        {
            string errorMessage = string.Empty;
            try
            {
                var urlPart = "Criminals/GetCriminals";

                var responseMgs = await _criminalApi.GetMessage<List<Criminal>>(urlPart);

                var criminalRecords = responseMgs;

                System.Threading.Thread.Sleep(2000);
                int draw = Convert.ToInt32(Request.Query["draw"]);
                if (draw <0)
                {
                    draw = 0;
                }
                // Data to be skipped
                // if 0 first "length" records will be fetched
                //if 1 second "length" of records will be fetched.
                int start = Convert.ToInt32(Request.Query["start"]);
                if (start <0)
                {
                    start = 0;
                }
                // records count to be fetched after skip
                int length = Convert.ToInt32(Request.Query["length"]);
                if (length <0)
                {
                    length = 0;
                }
                //Getting sort column name
                int sortColumnIdx = Convert.ToInt32(Request.Query["order[0][column]"]);
                string sortColumnName = Request.Query["columns[" + sortColumnIdx + "][name]"];

                //Sort column direction
                string sortColumnDirection = Request.Query["order[0][dir]"];

                // search value
                string searchValue = Request.Query["search[value]"].FirstOrDefault()?.Trim();

                //Record count matching search criteria
                int recordsFilteredCount = 0;
                if (searchValue !=null)
                {
                    recordsFilteredCount = criminalRecords.Where(c => c.first_name.Contains(searchValue) || c.last_name.Contains(searchValue)).Count();
                }
                //Total Record count
                int recordsTotalCount = criminalRecords.Count();

                //Filtered, sorted and paged data to be sent from server to view
                List<Criminal> filteredData = null;
                if (sortColumnDirection=="asc")
                {
                    if (searchValue != null)
                    {
                        filteredData = criminalRecords.Where(c => c.first_name.Contains(searchValue) || c.last_name.Contains(searchValue))
                                                .OrderBy(c => c.GetType().GetProperty(sortColumnName).GetValue(c))
                                                .Skip(start)
                                                .Take(length)
                                                .ToList();
                    }
                    
                }
                else
                {
                    if (searchValue !=null)
                    {
                        filteredData = criminalRecords.Where(c => c.first_name.Contains(searchValue)|| c.last_name.Contains(searchValue))
                                                .OrderByDescending(c => c.GetType().GetProperty(sortColumnName).GetValue(c))
                                                .Skip(start)
                                                .Take(length)
                                                .ToList();
                    }
                    
                }
                if (filteredData ==null)
                {
                    filteredData = criminalRecords;
                }

                //send data
                return Json(
                    new
                    {
                        data=filteredData,
                        draw=draw,
                        recordsFiltered=recordsFilteredCount,
                        recordsTotal=recordsTotalCount
                    }
                    );


            }
            catch (Exception ex)
            {
                errorMessage = "Something wrong has happened.";
                ViewBag.Message = errorMessage;
                return Json(errorMessage);
                //throw ex;
            }
            

        }
        // GET: Criminals/Details/5
        public async Task<IActionResult> Details1(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            string message;
            try
            {
                //Retrieve criminal record based on Criminal Number
                var criminalUrlPart = "Criminals/GetCriminal?id=" + id;

                var criminalResponseMgs = await _criminalApi.GetMessage<Criminal>(criminalUrlPart);
                if (criminalResponseMgs == null)
                {
                    return NotFound();
                }
                //return View(responseMgs);
                ViewBag.Criminal = criminalResponseMgs;

                //Retrieve crime record based on Criminal Number  CRM001
                var crimeUrlPart = "Crimes/GetCrimeAsync?criminalNo=" + criminalResponseMgs.criminal_no;

                var crimeResponseMgs = await _criminalApi.GetMessage<Crime>(crimeUrlPart);
                if (crimeResponseMgs==null)
                {
                    return NotFound();
                }
                //return View(responseMgs);
                ViewBag.Crimes = crimeResponseMgs;

                //Retrieve criminal Address record based on Criminal Number
                var criminalAddressUrlPart = "CriminalAddresses/GetCriminalAddresses?criminal_no=" +  criminalResponseMgs.criminal_no;

                var criminalAddressResponseMgs = await _criminalApi.GetMessage<CriminalAddress>(criminalAddressUrlPart);
                if (criminalAddressResponseMgs==null)
                {
                    return NotFound();
                }
                //return View(responseMgs);
                ViewBag.Address = criminalAddressResponseMgs;

                //Retrieve criminal History record based on Criminal Number
                var criminalHistoryUrlPart = "CriminalHistories/GetCriminalHistories?criminalNo=" +  criminalResponseMgs.criminal_no;

                var criminalHistoryResponseMgs = await _criminalApi.GetMessage<List<CriminalHistory>>(criminalHistoryUrlPart);
                if (criminalHistoryResponseMgs==null)
                {
                    return NotFound();
                }
                //return View(responseMgs);
                ViewBag.Histories = criminalHistoryResponseMgs;


            }
            catch (Exception)
            {

                message = "Something wrong happened";
                ViewBag.Message = message;
                return View();
            }

            return View();
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            string message;
            try
            {
                var status = "Current";
                var criminalNo = string.Empty;
                //Retrieve criminal record based on Criminal Number
                var criminalUrlPart = "Criminals/GetCriminal/" + id;

                var criminalResponseMgs = await _criminalApi.GetMessage<Criminal>(criminalUrlPart);
                if (criminalResponseMgs == null)
                {
                    return NotFound();
                }
                criminalNo = criminalResponseMgs.criminal_no;
                ViewBag.Criminal = criminalResponseMgs;

                //Retrieve crime record based on Criminal Number  CRM001
                //http://localhost:30001/api/Crimes/GetCrime/CRM001?status=Current
                var crimeUrlPart = "Crimes/GetCrime/" + criminalNo + "?status=" + status;

                var crimeResponseMgs = await _criminalApi.GetMessage<Crime>(crimeUrlPart);
                if (crimeResponseMgs==null)
                {
                    return NotFound();
                }
                ViewBag.Crimes = crimeResponseMgs;

                //Retrieve criminal Address record based on Criminal Number
                //http://localhost:30001/api/CriminalAddresses/GetCriminalAddress/CRM001?status=Current
                var criminalAddressUrlPart = "CriminalAddresses/GetCriminalAddress/" + criminalNo + "?status=" + status;

                var criminalAddressResponseMgs = await _criminalApi.GetMessage<CriminalAddress>(criminalAddressUrlPart);
                if (criminalAddressResponseMgs==null)
                {
                    return NotFound();
                }
                ViewBag.Address = criminalAddressResponseMgs;

                //Retrieve criminal History record based on Criminal Number
                //http://localhost:30001/api/CriminalHistories/GetCriminalHistories/CRM001
                var criminalHistoryUrlPart = "CriminalHistories/GetCriminalHistories/" + criminalNo;

                var criminalHistoryResponseMgs = await _criminalApi.GetMessage<List<CriminalHistory>>(criminalHistoryUrlPart);
                if (criminalHistoryResponseMgs==null)
                {
                    return NotFound();
                }
                ViewBag.Histories = criminalHistoryResponseMgs;


            }
            catch (Exception ex)
            {

                message = "Something wrong happened. Please and continue. " + Environment.NewLine + ex.Message;
                ViewBag.Message = message;
                return View();
            }

            return View();
        }
        
        // POST: Criminals/SaveCriminal
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveCriminal() //[FromBody]Criminal criminal
        {
            var message = "";
            var successMessage = "Saved Successfully";
            const string extJpeg = ".jpg";
            const string extPng = ".png";
            var uniqueFilename = Convert.ToString(Guid.NewGuid()).Replace("-","");
            var ext = string.Empty;
            var maximumSize = 3 * 1024 * 1024;

            var criminal = JsonConvert.DeserializeObject<Criminal>(Request.Form["model"].ToString());

            foreach (var file in Request.Form.Files)
            {
                if (file==null)
                {
                    ViewBag.Message = "Select an Image to continue.";
                    return BadRequest("Select an Image to continue.");
                    //return Json(new object[] { new object() });
                }
                ext = Path.GetExtension(file.FileName);
                if (ext.ToLower() == extJpeg || ext.ToLower() == extPng)
                {
                    if (file.Length>0 && file.Length <= maximumSize)
                    {
                        //Path.GetTempFileName();// 
                        var imgFilename = Path.GetFileName(file.FileName);
                        var fileExtension = Path.GetExtension(file.FileName);
                        var newFileName = string.Concat(uniqueFilename, fileExtension);

                        // Combines two strings into a path.
                        var filePath =
                 new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "ImageFiles")).Root + $@"\{newFileName}";

                        //var filePath = Path.Combine(_hostingEnvironment.WebRootPath, "ImageFiles", newFileName);

                        using (var fileSteam = new FileStream(filePath, FileMode.Create))
                        {
                            file.CopyTo(fileSteam);
                        }

                        criminal.image_path = newFileName;
                    }
                    else
                    {
                        message = "Check the image and make sure its in a valid size.";
                        ViewBag.Message = message;
                        return BadRequest("Check the image and make sure its in a valid size.");
                        //return Json(new object[] { new object() });
                    }

                }
                else
                {
                    message = "Make sure the image is in a correct format.";
                    ViewBag.Message = message;
                    return BadRequest("Make sure the image is in a correct format.");
                    //return Json(new object[] { new object() });
                }
            }

            try
            {
                criminal.CreatedBy = User.Identity.Name;
                criminal.CreatedOn = DateTime.Today;
                criminal.age = CalculateAge(criminal.date_of_birth);

                var urlPart = "Criminals/PostCriminal";
                var responseMsg = await _criminalApi.PostMessage(urlPart, criminal);
                var responses = responseMsg.Split('|');
                var responseMessage = responses[0].ToLower();
                var criminalNo = responses[1];

                if (responseMessage == successMessage.ToLower())
                {
                    CrimeTrackerUtility.CriminalNo = criminalNo;
                    message = successMessage;
                    return Ok(responseMessage);
                }

                return BadRequest("Something bad happened. Check and try again");
                //return Json(new object[] { new object() });
            }
            catch(Exception ex)
            {
                message = "Opp Something is wrong. " + ex.Message;
                ViewBag.Message = message;
                return BadRequest("Opp Something is wrong. " + ex.Message);
                //return Json(new object[] { new object() });
            }
        }
        private HttpContent CreateHttpContent<T>(T content)
        {
            var json = JsonConvert.SerializeObject(content, MicrosoftDateFormatSettings);
            return new StringContent(json, Encoding.UTF8, "application/json");
        }
        private static JsonSerializerSettings MicrosoftDateFormatSettings
        {
            get
            {
                return new JsonSerializerSettings
                {
                    DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
                };
            }
        }
        // GET: Criminals/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }
        private static int CalculateAge(DateTime dateOfBirth)
        {
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;

            return age;
        }

        // POST: Criminals/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Criminals/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Criminals/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
