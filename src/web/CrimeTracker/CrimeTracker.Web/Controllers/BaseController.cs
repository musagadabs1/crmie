﻿using CrimeTracker.Web.APIServices;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace CrimeTracker.Web.Controllers
{
    public class BaseController : Controller
    {
        readonly InternalAPI _apiClient;
        public BaseController(string dataUrl)
        {
            _apiClient = new InternalAPI(dataUrl);
        }

        public async Task<string> GetMessageFromServer(string urlPart)
        {
            var responseString = await _apiClient.GetMessage(urlPart);
            return responseString;
        }

        public async Task<string> MessageFromClientAsync(string urlPart, [FromBody] dynamic JsonMsg)
        {

            var responseString = await _apiClient.PostMessage(urlPart,
               JsonMsg != null ? JsonMsg.ToString() : "");
            return responseString;
        }

        public async Task<string> PostQueryToServerAsync(string urlPart)
        {
            int.TryParse(Request.Form["draw"], out int draw);
            int.TryParse(Request.Form["start"], out int start);
            int.TryParse(Request.Form["length"], out int length);
            string search = Request.Form["search[value]"];

            string JsonMsg = JsonConvert.SerializeObject
                (
                new
                {
                    draw,
                    start,
                    length
                }
                );
            JsonMsg = JsonMsg.Replace("}", "," + search.Replace("{", ""));

            var responseString = await _apiClient.PostMessage(urlPart, JsonMsg);
            return responseString;
        }

        internal async Task<string> PostMessage(string urlPart, string jsonMsg)
        {
            var result = await _apiClient.PostMessage(urlPart, jsonMsg);
            return result;
        }

        internal async Task<Y> PostToServer<Y>(string urlPart, string jsonMsg)
        {

            var result = await _apiClient.PostMessage<Y>(urlPart, jsonMsg);
            return result;
        }

        internal async Task<string> PostToServer<Y>(string urlPart, Y jsonMsg)
        {
            var result = await _apiClient.PostMessage<Y>(urlPart, jsonMsg);
            return result;
        }

        internal async Task<T> PostMessageWithData<T, U>(string urlPart, U data)
        {
            var result = await _apiClient.PostMessageWithData<T, U>(urlPart, data);
            return result;
        }

        [HttpDelete]
        public async Task<string> Delete(string urlPart)
        {
            return await _apiClient.Delete(urlPart);
        }
    }
}