﻿using CrimeTracker.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrimeTracker.Web.APIServices
{
    public class URL
    {
        public static string CriminalURL => WebUtility.GetAppConfigItem("CriminalsURL");
        //public static string Departments_HR => WebUtilility.GetAppConfigItem("Departments_HR");
        //public static string LeaveUrl => WebUtilility.GetAppConfigItem("LeaveUrl");
        //public static string Authentication => WebUtilility.GetAppConfigItem("Authentication");
        //public static string TimesheetURL => WebUtilility.GetAppConfigItem("TimesheetURL");
        //public static string ActivityUrl => WebUtilility.GetAppConfigItem("ActivityUrl");
        //public static string TravelUrl => WebUtilility.GetAppConfigItem("TravelUrl");

        //public static string ExpenseReimbursementUrl => WebUtilility.GetAppConfigItem("ExpenseReimbursementUrl");

        //public static string ProcUrl => WebUtilility.GetAppConfigItem("ProcUrl");

        //public static string PettyCashUrl => WebUtilility.GetAppConfigItem("PettyCashUrl");
        //public static string ProcurementUrl => WebUtilility.GetAppConfigItem("ProcurementUrl");
        //public static string VoucherReceiptUrl => WebUtilility.GetAppConfigItem("VoucherReceiptUrl");

        //public const string EmployeeURL = "http://localhost:3001/api/";
        //public const string Departments_HR = "http://localhost:3002/api/";
        //public const string LeaveUrl = "http://localhost:3003/api/";
        //public const string Authentication = "http://localhost:3004/api/";
        //public const string TimesheetURL = "http://localhost:3005/api";
        //public const string ActivityUrl = "http://localhost:5000/api/";
    }
}
