﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CrimeTracker.Web.APIServices
{
    public class InternalAPI
    {
        readonly string baseDataURL;

        HttpClient _apiClient => new HttpClient();

        public InternalAPI(string baseUrl)
        {
            baseDataURL = baseUrl;
        }

        public async Task<string> PostMessage(string urlPart, string jsonMsg)
        {
            string url = baseDataURL + urlPart;

            //Console.WriteLine(url);
            //Console.WriteLine(jsonMsg);

            StringContent stringContent = new StringContent(jsonMsg, Encoding.UTF8, "application/json");
            var response = await _apiClient.PostAsync(url, stringContent);
            return await response.Content.ReadAsStringAsync();
        }
        public async Task<T> PostAsync<T>(string urlPart, T content)
        {
            string url = baseDataURL + urlPart;
            var response = await _apiClient.PostAsync(url, CreateHttpContent<T>(content));
            //response.EnsureSuccessStatusCode();
            var data = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(data);
        }
        private static JsonSerializerSettings MicrosoftDateFormatSettings
        {
            get
            {
                return new JsonSerializerSettings
                {
                    DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
                };
            }
        }
        private HttpContent CreateHttpContent<T>(T content)
        {
            var json = JsonConvert.SerializeObject(content, MicrosoftDateFormatSettings);
            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        public async Task<T> PostMessage<T>(string urlPart, string jsonString)
        {
            string url = baseDataURL + urlPart;
            var response = await _apiClient.PostAsJsonAsync(url,
                                    new StringContent(jsonString, Encoding.UTF8, "application/json"));
            return await response.Content.ReadAsAsync<T>();
        }

        public async Task<string> PostMessage<T>(string urlPart, T data)
        {
            string url = baseDataURL + urlPart;
            var response = await _apiClient.PostAsJsonAsync(url, data);
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<T> PostMessageWithData<T, U>(string urlPart, U data)
        {
            string url = baseDataURL + urlPart;
            var response = await _apiClient.PostAsJsonAsync(url, data);
            if (response.StatusCode == System.Net.HttpStatusCode.Accepted ||
                response.StatusCode == System.Net.HttpStatusCode.Created ||
                response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return await response.Content.ReadAsAsync<T>();
            }
            else
            {
                throw new ApplicationException(await response.Content.ReadAsStringAsync());
            }
        }


        public async Task<string> PostFile(string urlPart, string JsonRequestData, List<IFormFile> formFiles)
        {
            string url = baseDataURL + urlPart;

            using (var client = new HttpClient())
            {
                var remote_address = new Uri(url);

                MultipartFormDataContent multiContent = new MultipartFormDataContent();

                try
                {
                    foreach (var file in formFiles)
                    {
                        byte[] data;
                        using (var br = new BinaryReader(file.OpenReadStream()))
                            data = br.ReadBytes((int)file.OpenReadStream().Length);

                        ByteArrayContent bytes = new ByteArrayContent(data);
                        multiContent.Add(bytes, "file", file.FileName);
                    }
                    multiContent.Add(new StringContent(JsonRequestData), "requestData");

                    var result = await client.PostAsync(remote_address, multiContent);
                    string resultString = await result.Content.ReadAsStringAsync();
                    return resultString;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    return "";
                }
            }
        }


        public async Task<string> GetMessage(string urlPart)
        {
            string url = baseDataURL + urlPart;
            try
            {
                var response = await _apiClient.GetAsync(url);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<T> GetMessage<T>(string urlPart)
        {
            try
            {
                string url = baseDataURL + urlPart;
                //new LoggerUtil.Logger().LogInfo(url, "getmessage()", "");

                var response = await _apiClient.GetAsync(url);
                return await response.Content.ReadAsAsync<T>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task<string> Delete(string urlPart)
        {
            string url = baseDataURL + urlPart;
            try
            {
                var response = await _apiClient.DeleteAsync(url);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
