using CrimeTracker.DAL.Data_Context;
using CrimeTracker.DAL.IServices;
using CrimeTracker.DAL.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Data.SqlClient;

namespace CrimeTracker.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var connStringBuilder = new SqlConnectionStringBuilder();
            connStringBuilder.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            connStringBuilder.UserID = Configuration["UserId"];
            connStringBuilder.Password = Configuration["Password"];
            //services.AddDbContext<CrimeTrackerDbContext>(options =>
                //options.UseSqlServer(
                    //Configuration.GetConnectionString("DefaultConnection")));

            services.AddDbContext<CrimeTrackerDbContext>(options =>
                options.UseSqlServer(connStringBuilder.ToString()));

            //services.AddIdentity<UserProfile, IdentityRole>().AddEntityFrameworkStores<CrimeTrackerDbContext>();

            services.AddControllersWithViews();
            services.AddSingleton<ICrimeTrackerService, CrimeTrackerService>();
            //services.AddMvc(opt =>
            //{
            //    var policy = new AuthorizationPolicyBuilder()
            //                    .RequireAuthenticatedUser()
            //                    .Build();
            //    opt.Filters.Add(new AuthorizeFilter(policy));
            //});
            services.ConfigureApplicationCookie(opts =>
            {
                //Cookies setting
                opts.Cookie.HttpOnly = true;
                opts.ExpireTimeSpan = TimeSpan.FromMinutes(5);

                opts.LoginPath = "/Identity/Account/Login";
                opts.AccessDeniedPath = "/Identity/Account/AccessDenied";
                opts.SlidingExpiration = true;
            });
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 8;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                
            });
            services.AddRazorPages();
            services.AddMvc().AddRazorPagesOptions(options =>
            {
                options.Conventions.AddAreaPageRoute("Identity", "/Account/Login", "");
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "areas",
                    pattern: "{area:exists}/{controller=Account}/{action=Login}");
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
            
        }
    }
}
