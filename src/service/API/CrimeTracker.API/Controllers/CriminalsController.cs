﻿using CrimeTracker.Core.Entities;
using CrimeTracker.DAL.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CrimeTracker.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CriminalsController : ControllerBase
    {
        //private readonly CrimeTrackerDbContext _context;
        private readonly ICrimeTrackerService _service; 
        public CriminalsController(ICrimeTrackerService service)
        {
            _service = service;
        }

        // GET: api/Criminals
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Criminal>>> GetCriminals()
        {
            try
            {
                return await _service.Criminal.GetCriminalsAsync();// _context.Criminals.ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET: api/Criminals/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Criminal>> GetCriminal(string id)
        {
            try
            {
                var criminal = await _service.Criminal.GetCriminalAsync(id);// _context.Criminals.FindAsync(id);

                if (criminal == null)
                {
                    return NotFound();
                }

                return criminal;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // PUT: api/Criminals/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCriminal(string id, Criminal criminal)
        {
            try
            {
                if (id != criminal.criminal_no)
                {
                    return BadRequest();
                }

                _service.Criminal.Update(criminal);
                await _service.Criminal.SaveAsync();
            }
            catch (Exception ex)
            {
                    throw ex;
            }

            return Ok($"Updated Successfully|{criminal.criminal_no}");
        }

        // POST: api/Criminals
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Criminal>> PostCriminal(Criminal criminal)
        {
            try
            {
                _service.Criminal.AddCriminal(criminal);

                await _service.Criminal.SaveAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok($"Saved Successfully|{criminal.criminal_no}");
        }

        // DELETE: api/Criminals/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Criminal>> DeleteCriminal(string id)
        {
            try
            {
                var criminal = await _service.Criminal.GetCriminalAsync(id);
                if (criminal == null)
                {
                    return NotFound();
                }
                _service.Criminal.DeleteCriminal(criminal);
                await _service.Criminal.SaveAsync();

                return criminal;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
