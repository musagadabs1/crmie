﻿using CrimeTracker.Core.Entities;
using CrimeTracker.DAL.IServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CrimeTracker.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CriminalHistoriesController : ControllerBase
    {
        //private readonly CrimeTrackerDbContext _context;
        private readonly ICrimeTrackerService _service;
        public CriminalHistoriesController(ICrimeTrackerService service)
        {
            _service = service; // context;
        }

        // GET: api/CriminalHistories
        [HttpGet("{criminalNo}")]
        public async Task<ActionResult<IEnumerable<CriminalHistory>>> GetCriminalHistories(string criminalNo)
        {
            try
            {
                return await _service.CrimeHistory.GetCriminalHistoriesAsync(criminalNo); // _context.CriminalHistories.ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // GET: api/CriminalHistories/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CriminalHistory>> GetCriminalHistory(string id)
        {
            try
            {
                var criminalHistory = await _service.CrimeHistory.GetCriminalHistoryAsync(id); // _context.CriminalHistories.FindAsync(id);

                if (criminalHistory == null)
                {
                    return NotFound();
                }

                return criminalHistory;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // PUT: api/CriminalHistories/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCriminalHistory(string id, CriminalHistory criminalHistory)
        {
            if (id != criminalHistory.criminal_no)
            {
                return BadRequest();
            }

            _service.CrimeHistory.UpdateCriminalHistory(criminalHistory);// _context.Entry(criminalHistory).State = EntityState.Modified;

            try
            {
                await _service.CrimeHistory.SaveAsync();// _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CriminalHistoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CriminalHistories
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CriminalHistory>> PostCriminalHistory(CriminalHistory criminalHistory)
        {

            try
            {
                _service.CrimeHistory.AddCriminalHistory(criminalHistory); // _context.CriminalHistories.Add(criminalHistory);
                await _service.CrimeHistory.SaveAsync(); // _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            

            return CreatedAtAction("GetCriminalHistory", new { id = criminalHistory.id }, criminalHistory);
        }

        // DELETE: api/CriminalHistories/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CriminalHistory>> DeleteCriminalHistory(string id)
        {
            try
            {
                var criminalHistory = await _service.CrimeHistory.GetCriminalHistoryAsync(id); // _context.CriminalHistories.FindAsync(id);
                if (criminalHistory == null)
                {
                    return NotFound();
                }

                _service.CrimeHistory.DeleteCriminalHistory(criminalHistory); // _context.CriminalHistories.Remove(criminalHistory);
                await _service.CrimeHistory.SaveAsync();// _context.SaveChangesAsync();

                return criminalHistory;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private bool CriminalHistoryExists(string id)
        {
            try
            {
                var crimeHistory = _service.CrimeHistory.GetCriminalHistory(id);
                if (crimeHistory != null)
                {
                    return true;
                }
                return false;// _context.CriminalHistories.Any(e => e.id == id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
