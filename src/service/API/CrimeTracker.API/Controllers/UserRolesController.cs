﻿using CrimeTracker.Core.Entities;
using CrimeTracker.DAL.Data_Context;
using CrimeTracker.DAL.IServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrimeTracker.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserRolesController : ControllerBase
    {
        //private readonly CrimeTrackerDbContext _context;
        private readonly ICrimeTrackerService _service;
        public UserRolesController(ICrimeTrackerService service)
        {
            _service = service;
        }

        // GET: api/UserRoles
        [HttpGet]
        public ActionResult<IEnumerable<UserRole>> GetUserRoles()
        {
            try
            {
                return _service.User.GetRoles();//.UserRoles.ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // GET: api/UserRoles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserRole>> GetUserRole(string id)
        {
            var userRole = await _service.UserRole.GetUserRoleAsync(id);

            if (userRole == null)
            {
                return NotFound();
            }

            return userRole;
        }

        // PUT: api/UserRoles/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserRole(int id, UserRole userRole)
        {
            if (id != userRole.Id)
            {
                return BadRequest();
            }

            _service.UserRole.UpdateUserRole(userRole);//.State = EntityState.Modified;

            try
            {
                await _service.UserRole.SaveAsync();
            }
            catch (Exception ex)
            {
               throw ex;
            }

            return NoContent();
        }

        // POST: api/UserRoles
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<UserRole>> PostUserRole(UserRole userRole)
        {
            _service.UserRole.AddUserRole(userRole);
            await _service.UserRole.SaveAsync();

            return Ok($"Updated Successfully|{userRole.Name}");
        }

        // DELETE: api/UserRoles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UserRole>> DeleteUserRole(string id)
        {
            var userRole = _service.UserRole.GetUserRole(id);//.UserRoles.FindAsync(id);
            if (userRole == null)
            {
                return NotFound();
            }

            _service.UserRole.DeleteUser(userRole);
            await _service.UserRole.SaveAsync();//.SaveChangesAsync();

            return userRole;
        }

        //private bool UserRoleExists(int id)
        //{
        //    return _context.UserRoles.Any(e => e.Id == id);
        //}
    }
}
