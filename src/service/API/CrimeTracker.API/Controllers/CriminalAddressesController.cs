﻿using CrimeTracker.Core.Entities;
using CrimeTracker.DAL.IServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CrimeTracker.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CriminalAddressesController : ControllerBase
    {
        //private readonly CrimeTrackerDbContext _context;
        private readonly ICrimeTrackerService _service;
        public CriminalAddressesController(ICrimeTrackerService service)
        {
            _service = service;
        }
        // GET: api/CriminalAddresses
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CriminalAddress>>> GetCriminalAddresses(string criminal_no)
        {
            try
            {
                return await _service.CriminalAddress.GetCriminalAddressesAsync(criminal_no);// _context.CriminalAddresses.ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // GET: api/CriminalAddresses/5
        [HttpGet("{criminalNo}")]
        public async Task<ActionResult<CriminalAddress>> GetCriminalAddress(string criminalNo,string status="")
        {
            CriminalAddress criminalAddress = null;
            try
            {
                if (status=="")
                {
                    criminalAddress = await _service.CriminalAddress.GetCriminalAddressAsync(criminalNo);// _context.CriminalAddresses.FindAsync(id);
                }
                else
                {
                    criminalAddress = await _service.CriminalAddress.GetCriminalAddressAsync(criminalNo,status);// _context.CriminalAddresses.FindAsync(id);
                }

                if (criminalAddress == null)
                {
                    return NotFound();
                }

                return criminalAddress;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // PUT: api/CriminalAddresses/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCriminalAddress(string id, CriminalAddress criminalAddress)
        {
            if (id != criminalAddress.criminal_no)
            {
                return BadRequest();
            }

            _service.CriminalAddress.Update(criminalAddress);
            //_context.Entry(criminalAddress).State = EntityState.Modified;

            try
            {
                await _service.CriminalAddress.SaveAsync();// _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CriminalAddressExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CriminalAddresses
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CriminalAddress>> PostCriminalAddress(CriminalAddress criminalAddress)
        {
            try
            {
                var criminalAddressToUpdate = await _service.CriminalAddress.GetCriminalAddressAsync(x => x.criminal_no == criminalAddress.criminal_no && x.Status == "Current");
                if (criminalAddressToUpdate != null)
                {
                    criminalAddressToUpdate.Status = "Previous";
                    _service.CriminalAddress.Update(criminalAddressToUpdate);
                    //_service.Crime.Save();
                }

                criminalAddress.Status = "Current";

                _service.CriminalAddress.AddCriminalAddress(criminalAddress); // _context.CriminalAddresses.Add(criminalAddress);
                await _service.CriminalAddress.SaveAsync(); // _context.SaveChangesAsync();

                return Ok($"Saved Successfully|{criminalAddress.criminal_no}");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // DELETE: api/CriminalAddresses/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CriminalAddress>> DeleteCriminalAddress(string id)
        {
            try
            {
                var criminalAddress = await _service.CriminalAddress.GetCriminalAddressAsync(id);// _context.CriminalAddresses.FindAsync(id);
                if (criminalAddress == null)
                {
                    return NotFound();
                }

                _service.CriminalAddress.DeleteCriminalAddress(criminalAddress); // _context.CriminalAddresses.Remove(criminalAddress);
                await _service.CriminalAddress.SaveAsync(); // _context.SaveChangesAsync();

                return criminalAddress;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private bool CriminalAddressExists(string id)
        {

            try
            {
                var crimeAdd = _service.CriminalAddress.GetCriminalAddress(id);
                if (crimeAdd != null)
                {
                    return true;
                }
                return false;// _context.CriminalAddresses.Any(e => e.Id == id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
