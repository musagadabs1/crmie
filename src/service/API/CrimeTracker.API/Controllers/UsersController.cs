﻿using CrimeTracker.API.Helper;
using CrimeTracker.API.Models;
using CrimeTracker.Core.Entities;
using CrimeTracker.DAL.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CrimeTracker.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ICrimeTrackerService _service;
        private readonly AppSettings _appSettings;
        //private readonly CrimeTrackerDbContext _context;

        public UsersController(ICrimeTrackerService service, IOptions<AppSettings> appSettings)
        {
            _service = service;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType(typeof(object), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Authenticate([FromBody]LoginModel model)
        {
            var user = _service.User.Authenticate(model.Email, model.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });
            else
            {
                user.LastLoginDate = DateTime.Now;
                _service.User.UpdateUser(user);
            }
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, model.Email) //user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            // return basic user info (without password) and token to store client side
            return Ok(
                new
                {
                    user.Id,
                    user.FirstName,
                    user.LastName,
                    user.Username,
                    Token = tokenString,
                    model.Email,
                    UserName = model.Email,
                    user.LastLoginDate
                });
        }

        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType(typeof(User), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Register([FromBody]RegisterModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = new User
                    {
                        CreationDate = DateTime.Now,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Username = model.Username,
                        Email = model.Email,
                        Status = ProfileStatus.Enabled,
                        CreatedBy = model.CreatedBy,
                    };
                    // save 
                    var u = await _service.User.CreateUser(user, model.Password, model.RoleName);
                    return Ok(u);
                }
                else
                {
                    var errors = ModelState.Values.SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage);

                    return BadRequest(errors);
                }
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<User>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult UsersInRole(string rolename)
        {
            var user = _service.User.UsersInRole(rolename);
            return Ok(user);
        }

        [HttpGet]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult IsInRole(string username, string rolename)
        {
            bool inRole = _service.User.IsInRole(username, rolename);
            return Ok(inRole);
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<string>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetRoles()
        {
            var roles = _service.User.GetRoles()
                .Select(x => x.Name)
                .ToList();
            return Ok(roles);
        }

        [HttpPost]
        public async Task<IActionResult> AddToRole(string username, string rolename)
        {
            UserRole roles = await _service.User.AssignRole(username, rolename);
            return Ok(roles);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(string userid, string password)
        {
            string result = await _service.User.Changepassowrd(userid, password);
            return Ok(result);
        }
        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            try
            {
                return await _service.User.GetUsersAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(int id)
        {
            var user = await _service.User.GetUserAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(int id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            _service.User.UpdateUser(user);

            try
            {
                await _service.User.SaveAsync();
            }
            catch (Exception ex)
            {
                    throw ex;
            }

            return NoContent();
        }

        // POST: api/Users
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(User user)
        {
            _service.User.AddUser(user);
            try
            {
                await _service.User.SaveAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok($"Saved Successfully|{user.UserRoleId}");
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> DeleteUser(int id)
        {
            try
            {
                var user = await _service.User.GetUserAsync(id);
                if (user == null)
                {
                    return NotFound();
                }

                _service.User.DeleteUser(user);
                await _service.User.SaveAsync();

                return user;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //private bool UserExists(int id)
        //{
        //    return _context.Users.Any(e => e.Id == id);
        //}
    }
    public class UpdateModel
    {
        public string Rolename { get; set; }
        public int UserId { get; set; }
    }
}
