﻿using CrimeTracker.Core.Entities;
using CrimeTracker.DAL.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CrimeTracker.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CrimesController : ControllerBase
    {
        private readonly ICrimeTrackerService _service;
        //private readonly CrimeTrackerDbContext _context;

        public CrimesController(ICrimeTrackerService  service)
        {
            _service = service;
        }

        // GET: api/Crimes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Crime>>> GetCrimes(string criminalNo)
        {
            try
            {
                return await _service.Crime.GetCrimesAsync(criminalNo);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        // GET: api/Crimes/5
        [HttpGet("{criminalNo}")]
        public async Task<ActionResult<Crime>> GetCrime(string criminalNo,string status="")
        {
            try
            {
                Crime crime = null;
                if (status=="")
                {
                  crime = await _service.Crime.GetCrimeAsync(criminalNo);// _context.Crimes.FindAsync(id);
                }
                else
                {
                    crime = await _service.Crime.GetCrimeAsync(criminalNo,status);// _context.Crimes.FindAsync(id);
                }
                

                if (crime == null)
                {
                    return NotFound();
                }

                return crime;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // PUT: api/Crimes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCrime(string id, Crime crime)
        {
            if (id != crime.CrimeNo)
            {
                return BadRequest();
            }

            try
            {
                await _service.Crime.UpdateAsync(crime);
                return Ok($"Updated Successfully|{crime.CriminalNo}");
            }
            catch (Exception ex)
            {
                    throw ex;
            }

            
        }

        // POST: api/Crimes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Crime>> PostCrime(Crime crime)
        {
            var crimeHistory = new CriminalHistory
            {
                charge_and_disposition = crime.CrimeName,
                city = crime.PlaceOfArrest,
                date = crime.DateOfArrest,
                criminal_no = crime.CriminalNo,
                CreatedBy = crime.CreatedBy,
                CreatedOn = crime.CreatedOn,
                ModifiedBy = crime.ModifiedBy,
                IpAddress = crime.IpAddress
            };

            _service.CrimeHistory.AddCriminalHistory(crimeHistory);
            var crimeToUpdate = await _service.Crime.GetCrimeAsync(x => x.CriminalNo==crime.CriminalNo && x.Status=="Current");
            if (crimeToUpdate !=null)
            {
                crimeToUpdate.Status = "Previous";
                _service.Crime.Update(crimeToUpdate);
                //_service.Crime.Save();
            }

            crime.Status = "Current";
            _service.Crime.AddCrime(crime);
            try
            {
                _service.CrimeHistory.Save();
                await _service.Crime.SaveAsync();
                return Ok($"Saved Successfully|{crime.CriminalNo}");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            
        }

        // DELETE: api/Crimes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Crime>> DeleteCrime(string id)
        {
            try
            {
                var crime = await _service.Crime.GetCrimeAsync(id);
                if (crime == null)
                {
                    return NotFound();
                }

                _service.Crime.DeleteCrime(crime);
                await _service.Crime.SaveAsync();

                return crime;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
