﻿using CrimeTracker.Core.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace CrimeTracker.DAL.Data_Context
{
    public class CrimeTrackerDbContext:IdentityDbContext
    {
        public IConfiguration Configuration { get; set; }
        public CrimeTrackerDbContext(DbContextOptions<CrimeTrackerDbContext> options,IConfiguration configuration) : base(options)
        {
            Configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var environment = string.Empty;

            base.OnConfiguring(optionsBuilder);

            var connStringBuilder = new SqlConnectionStringBuilder();

            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json", optional: true, reloadOnChange: true);

            var config = builder.Build();

            connStringBuilder.ConnectionString = Configuration.GetConnectionString("CrimeTrackerConnection");

            //connStringBuilder.UserID= Configuration["UserId"];
            //connStringBuilder.Password = Configuration["Password"];

            optionsBuilder.UseSqlServer(connStringBuilder.ToString());
        }
        public CrimeTrackerDbContext()
        {

        }
        public virtual DbSet<Crime> Crimes { get; set; }
        public virtual DbSet<Criminal> Criminals { get; set; }
        public virtual DbSet<CriminalAddress> CriminalAddresses { get; set; }
        public virtual DbSet<CriminalHistory> CriminalHistories { get; set; }
        public virtual DbSet<BiometricInfo> BiometricInfos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
