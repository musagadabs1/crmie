﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CrimeTracker.DAL.Migrations
{
    public partial class biometricChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "biometric_info");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "biometric_info",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    created_by = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    criminalNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    date_created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    fingerPosition = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    imageDPI = table.Column<int>(type: "int", nullable: false),
                    imageHeight = table.Column<int>(type: "int", nullable: false),
                    imageQuality = table.Column<int>(type: "int", nullable: false),
                    imageWidth = table.Column<int>(type: "int", nullable: false),
                    manufacturer = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    model = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    serialNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    template = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_biometric_info", x => x.id);
                });
        }
    }
}
