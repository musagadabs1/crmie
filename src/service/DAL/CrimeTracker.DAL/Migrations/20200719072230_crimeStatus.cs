﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrimeTracker.DAL.Migrations
{
    public partial class crimeStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "crime_details",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "crime_details");
        }
    }
}
