﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrimeTracker.DAL.Migrations
{
    public partial class AddStatusInCriminalAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "criminal_address",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "criminal_address");
        }
    }
}
