﻿using CrimeTracker.Core.Entities;
using CrimeTracker.DAL.Data_Context;
using CrimeTracker.DAL.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.Services
{
    public class UserRoleRepository : IUserRoleRepository
    {
        private CrimeTrackerDbContext _context = new CrimeTrackerDbContext();
        public UserRole AddUserRole(UserRole userRole)
        {
            try
            {
                _context.UserRoles.Add(userRole);
                return userRole;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<UserRole> AddUserRoleAsync(UserRole userRole)
        {
            try
            {
                await _context.UserRoles.AddAsync(userRole);
                return userRole;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteUser(UserRole userRole)
        {
            try
            {
                _context.UserRoles.Remove(userRole);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Dispose()
        {
            try
            {
                _context.Dispose();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public UserRole GetUserRole(string userId)
        {
            try
            {
                return _context.UserRoles.Where(x => x.Name == userId).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<UserRole> GetUserRoleAsync(string userId)
        {
            try
            {
                return await _context.UserRoles.Where(x => x.Name == userId).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<UserRole> GetUserRoles()
        {
            try
            {
                return _context.UserRoles.ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<UserRole> GetUserRoles(Expression<Func<UserRole, bool>> expression)
        {
            try
            {
                return _context.UserRoles.Where(expression).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<UserRole>> GetUserRolesAsync()
        {
            try
            {
                return await _context.UserRoles.ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<UserRole>> GetUserRolesAsync(Expression<Func<UserRole, bool>> expression)
        {
            try
            {
                return await _context.UserRoles.Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int Save()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<int> SaveAsync()
        {
            try
            {
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void UpdateUserRole(UserRole userRole)
        {
            try
            {
                _context.UserRoles.Update(userRole);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
