﻿using CrimeTracker.Core.Entities;
using CrimeTracker.DAL.Data_Context;
using CrimeTracker.DAL.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.Services
{
    public class CriminalRepository : ICriminalRepository
    {
        private CrimeTrackerDbContext _context = new CrimeTrackerDbContext();
        public Criminal AddCriminal(Criminal criminal)
        {
            try
            {
                _context.Criminals.Add(criminal);
                return criminal;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<Criminal> AddCriminalAsync(Criminal criminal)
        {
            try
            {
                await _context.Criminals.AddAsync(criminal);
                return criminal;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteCriminal(Criminal criminal)
        {
            try
            {
                _context.Criminals.Remove(criminal);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Dispose()
        {
            try
            {
                _context.Dispose();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public Criminal GetCriminal(string id)
        {
            try
            {
                return _context.Criminals.Find(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<Criminal> GetCriminals()
        {
            try
            {
                return _context.Criminals.ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<Criminal> GetCriminalAsync(string id)
        {
            try
            {
                return await _context.Criminals.FindAsync(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<Criminal> GetCriminals(Expression<Func<Criminal, bool>> expression)
        {
            try
            {
                return _context.Criminals.Where(expression).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<Criminal>> GetCriminalsAsync()
        {
            try
            {
                return await _context.Criminals.ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<Criminal>> GetCriminalsAsync(Expression<Func<Criminal, bool>> expression)
        {
            try
            {
                return await _context.Criminals.Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int Save()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<int> SaveAsync()
        {
            try
            {
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Update(Criminal criminal)
        {
            try
            {
                _context.Criminals.Update(criminal);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task UpdateAsync(Criminal criminal)
        {
            try
            {
                _context.Criminals.Update(criminal);
               await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public Criminal GetCriminal(Expression<Func<Criminal, bool>> expression)
        {
            try
            {
                return _context.Criminals.Where(expression).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
