﻿using CrimeTracker.DAL.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.Services
{
    public class CrimeTrackerService:ICrimeTrackerService
    {
        public CrimeTrackerService()
        {
            CrimeHistory = new CriminalHistoryRepository();
            Crime = new CrimeRepository();
            Criminal = new CriminalRepository();
            CriminalAddress = new CriminalAddressRepository();
            //CriminalImage = new CriminalImageRepository();
        }
        public ICrimeHistoryRepository  CrimeHistory { get;}

        public ICrimeRepository Crime { get; }

        public ICriminalRepository Criminal { get; }

        public ICriminalAddressRepository CriminalAddress { get; }

        //public ICriminalImageRepository CriminalImage { get; }

        public IUserRepository User { get; }

        public IUserRoleRepository UserRole { get; }

        public int Complete()
        {
            throw new NotImplementedException();
        }

        public Task<int> CompleteAsync()
        {
            throw new NotImplementedException();
        }
    }
}
