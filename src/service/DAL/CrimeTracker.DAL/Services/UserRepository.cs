﻿using CrimeTracker.Core.Entities;
using CrimeTracker.Core.Utility;
using CrimeTracker.DAL.Data_Context;
using CrimeTracker.DAL.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.Services
{
    public class UserRepository : IUserRepository
    {
        private CrimeTrackerDbContext _context = new CrimeTrackerDbContext();
        public User AddUser(User user)
        {
            try
            {
                _context.Users.Add(user);
                return user;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<User> AddUserAsync(User user)
        {
            try
            {
                await _context.Users.AddAsync(user);
                return user;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<UserRole> AssignRole(string username, string rolename)
        {
            try
            {
                var role = GetRoleByRoleName(rolename);
                if (role == null)
                    throw new ApplicationException("Invalid role supplied");

                var user = GetUser(username);
                user.UserRole = role;

                UpdateUser(user);
                await _context.SaveChangesAsync();

                return role;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public User Authenticate(string username, string password)
        {
            try
            {
                 var user = GetUser(username);
                // check if username exists
                if (user == null)
                    return null;

                // check if password is correct
                return VerifyPassword(user.Password, password) ? user : null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<string> Changepassowrd(string userid, string password)
        {
            try
            {
                string returnMsg = "Updated succesfully";

                if (!string.IsNullOrEmpty(password))
                {
                    var user = _context.Users.FirstOrDefault(u => u.Username == userid);
                    user.Password =Utils.GetMd5Hash(password);
                    UpdateUser(user);
                    await _context.SaveChangesAsync();
                }
                else
                {
                    returnMsg = "invalid password supplied";
                }
                return returnMsg;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<User> CreateUser(User user, string password, string roleName)
        {
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            if (GetUser(user.Username) != null)
                throw new AppException("Username '" + user.Username + "' is already taken");
            var userrole = _context.UserRoles.FirstOrDefault(x => x.Name.ToLower() == roleName.ToLower());

            user.UserRole = userrole ?? throw new ApplicationException("Invalid role speficied");

            user.Password = HashPassword(password);
            try
            {
                _context.Users.Add(user);
                await _context.SaveChangesAsync();
                return user;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void DeleteUser(User user)
        {
            try
            {
                _context.Users.Remove(user);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Dispose()
        {
            try
            {
                _context.Dispose();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<UserRole> GetRoles()
        {
            try
            {
                return _context.UserRoles.ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public User GetUser(string userId)
        {
            try
            {
                return _context.Users.Where(x => x.Username == userId).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public User GetUser(int userRoleId)
        {
            try
            {
                return _context.Users.Where(x => x.UserRoleId == userRoleId ).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<User> GetUserAsync(string userId)
        {
            try
            {
                return await _context.Users.Where(x => x.Username == userId).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<User> GetUserAsync(int userRoleId)
        {
            try
            {
                return await _context.Users.Where(x => x.UserRoleId == userRoleId).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<User> GetUsers()
        {
            try
            {
                return _context.Users.ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<User> GetUsers(Expression<Func<User, bool>> expression)
        {
            try
            {
                return _context.Users.Where(expression).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<User>> GetUsersAsync()
        {
            try
            {
                return await _context.Users.ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<User>> GetUsersAsync(Expression<Func<User, bool>> expression)
        {
            try
            {
                return await _context.Users.Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool IsInRole(string username, string rolename)
        {
            try
            {
                var user = GetUser(username);
                if (user != null)
                {
                    user =  _context.Users.FirstOrDefault(u => u.Username == username.ToLower()
                  && u.UserRole.Functions.Contains(rolename.ToLower()));
                }
                return user != null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void ResetPassword(int userid)
        {
            var user = GetUser(userid);
            //send change password
        }

        public int Save()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<int> SaveAsync()
        {
            try
            {
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void UpdateUser(User user)
        {
            try
            {
                _context.Users.Update(user);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<User> UpdateUser(int userid, string rolename)
        {
            var user = GetUser(userid);

            if (user == null)
                throw new AppException("User not found");

            var userrole = _context.UserRoles.FirstOrDefault(x => x.Name.ToLower() == rolename.ToLower());
            user.UserRole = userrole ?? throw new ApplicationException("Invalid role speficied");

            UpdateUser(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public List<User> UsersInRole(string rolename)
        {
            try
            {
                var users = _context.Users.
                Where(u => u.UserRole.Functions.Contains(rolename.ToLower())).ToList();
                return users;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string HashPassword(string password)
        {
            using (var md5 = MD5.Create())
            {
                return GetMd5Hash(md5, password);
            }
        }

        bool VerifyPassword(string hashedPassword, string providedPassword)
        {
            using (var md5 = MD5.Create())
            {
                return VerifyMd5Hash(md5, providedPassword, hashedPassword);
                //  ? PasswordVerificationResult.Success : PasswordVerificationResult.Failed;
            }
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public UserRole GetRoleByRoleName(string roleName)
        {
            try
            {
                return _context.UserRoles.FirstOrDefault(x => x.Name.ToLower().Equals(roleName.ToLower()));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
