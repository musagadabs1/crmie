﻿using CrimeTracker.Core.Entities;
using CrimeTracker.DAL.Data_Context;
using CrimeTracker.DAL.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.Services
{
    public class CriminalAddressRepository : ICriminalAddressRepository
    {
        private CrimeTrackerDbContext _context = new CrimeTrackerDbContext();
        public CriminalAddress AddCriminalAddress(CriminalAddress criminalAddress)
        {
            try
            {
                _context.CriminalAddresses.Add(criminalAddress);
                return criminalAddress;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<CriminalAddress> AddCriminalAddressAsync(CriminalAddress criminalAddress)
        {
            try
            {
                 await _context.CriminalAddresses.AddAsync(criminalAddress);
                return criminalAddress;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteCriminalAddress(CriminalAddress criminalAddress)
        {
            try
            {
                _context.CriminalAddresses.Remove(criminalAddress);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Dispose()
        {
            try
            {
                _context.Dispose();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public CriminalAddress GetCriminalAddress(string criminalNo)
        {
            try
            {
                return _context.CriminalAddresses.Where(x => x.criminal_no == criminalNo).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<CriminalAddress> GetCriminalAddresses()
        {
            try
            {
                return _context.CriminalAddresses.ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<CriminalAddress> GetCriminalAddresses(Expression<Func<CriminalAddress, bool>> expression)
        {
            try
            {
                return _context.CriminalAddresses.Where(expression).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        
        public async Task<CriminalAddress> GetCriminalAddressAsync(string criminalNo, string status = "")
        {
            try
            {
                if (status=="")
                {
                    return await _context.CriminalAddresses.Where(x => x.criminal_no == criminalNo).FirstOrDefaultAsync();
                }
                else
                {
                    return await _context.CriminalAddresses.Where(x => x.criminal_no == criminalNo && x.Status==status).FirstOrDefaultAsync();
                }
                
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<CriminalAddress>> GetCriminalAddressesAsync()
        {
            try
            {
                return await _context.CriminalAddresses.ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<CriminalAddress>> GetCriminalAddressesAsync(Expression<Func<CriminalAddress, bool>> expression)
        {
            try
            {
                return await _context.CriminalAddresses.Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int Save()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<int> SaveAsync()
        {
            try
            {
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Update(CriminalAddress criminal)
        {
            try
            {
                _context.CriminalAddresses.Update(criminal);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task UpdateAsync(CriminalAddress criminal)
        {
            try
            {
                _context.CriminalAddresses.Update(criminal);
               await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<CriminalAddress>> GetCriminalAddressesAsync(string criminalNo)
        {
            try
            {
                return await _context.CriminalAddresses.Where(x => x.criminal_no == criminalNo).ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<CriminalAddress> GetCriminalAddressAsync(Expression<Func<CriminalAddress, bool>> expression)
        {
            try
            {
                return await _context.CriminalAddresses.Where(expression).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
