﻿using CrimeTracker.Core.Entities;
using CrimeTracker.DAL.Data_Context;
using CrimeTracker.DAL.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.Services
{
    public class CrimeRepository : ICrimeRepository
    {
        private CrimeTrackerDbContext _context = new CrimeTrackerDbContext();
        public Crime AddCrime(Crime crime)
        {
            try
            {
                _context.Crimes.Add(crime);
                return crime;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<Crime> AddCrimeAsync(Crime crime)
        {
            try
            {
               await _context.Crimes.AddAsync(crime);
                return crime;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteCrime(Crime crime)
        {
            try
            {
                _context.Crimes.Remove(crime);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Dispose()
        {
            try
            {
                _context.Dispose();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public Crime GetCrime(string criminalNo)
        {
            try
            {
                return _context.Crimes.Where(x => x.CriminalNo == criminalNo).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<Crime> GetCrimeAsync(Expression<Func<Crime, bool>> expression)
        {
            try
            {
                return await _context.Crimes.Where(expression).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<Crime> GetCrimeAsync(string criminalNo,string status="")
        {
            try
            {
                if (status=="")
                {
                    return await _context.Crimes.Where(x => x.CriminalNo == criminalNo).FirstOrDefaultAsync();
                }
                return await _context.Crimes.Where(x => x.CriminalNo == criminalNo && x.Status== status).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<Crime> GetCrimes(string criminalNo)
        {
            try
            {
                return _context.Crimes.Where(x => x.CriminalNo == criminalNo).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<Crime> GetCrimes(Expression<Func<Crime, bool>> expression)
        {
            try
            {
                return _context.Crimes.Where(expression).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<Crime>> GetCrimesAsync(string criminalNo)
        {
            try
            {
                return await _context.Crimes.Where(x => x.CriminalNo == criminalNo).ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<Crime>> GetCrimesAsync(Expression<Func<Crime, bool>> expression)
        {
            try
            {
                return await _context.Crimes.Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int Save()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<int> SaveAsync()
        {
            try
            {
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Update(Crime crime)
        {
            try
            {
                _context.Crimes.Update(crime);
                //return _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task UpdateAsync(Crime crime)
        {
            try
            {
                _context.Crimes.Update(crime);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
