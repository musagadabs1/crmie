﻿using CrimeTracker.Core.Entities;
using CrimeTracker.DAL.Data_Context;
using CrimeTracker.DAL.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.Services
{
    public class CriminalHistoryRepository : ICrimeHistoryRepository
    {
        private CrimeTrackerDbContext _context = new CrimeTrackerDbContext();
        public CriminalHistory AddCriminalHistory(CriminalHistory criminal)
        {
            try
            {
                _context.CriminalHistories.Add(criminal);
                return criminal;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<CriminalHistory> AddCriminalHistoryAsync(CriminalHistory criminal)
        {
            try
            {
               await _context.CriminalHistories.AddAsync(criminal);
                return criminal;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteCriminalHistory(CriminalHistory criminal)
        {
            try
            {
                _context.CriminalHistories.Remove(criminal);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Dispose()
        {
            try
            {
                _context.Dispose();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<CriminalHistory> GetCriminalHistories(string criminalNo)
        {
            try
            {
                return _context.CriminalHistories.Where(x => x.criminal_no == criminalNo).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<CriminalHistory> GetCriminalHistories(Expression<Func<CriminalHistory, bool>> expression)
        {
            try
            {
                return _context.CriminalHistories.Where(expression).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<CriminalHistory> GetCriminalHistoryAsync(string criminalNo)
        {
            try
            {
                return await _context.CriminalHistories.Where(x => x.criminal_no == criminalNo).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<CriminalHistory>> GetCriminalHistoriesAsync(string criminalNo)
        {
            try
            {
                return await _context.CriminalHistories.Where(x => x.criminal_no == criminalNo).ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int Save()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<int> SaveAsync()
        {
            try
            {
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public CriminalHistory GetCriminalHistory(string criminalNo)
        {
            try
            {
                return _context.CriminalHistories.Where(x => x.criminal_no == criminalNo).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<CriminalHistory>> GetCriminalHistoriesAsync(Expression<Func<CriminalHistory, bool>> expression)
        {
            try
            {
                return await _context.CriminalHistories.Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void UpdateCriminalHistory(CriminalHistory criminal)
        {
            try
            {
                _context.CriminalHistories.Update(criminal);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
