﻿using CrimeTracker.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.IServices
{
    public interface ICrimeRepository
    {
        void Dispose();
        Task<int> SaveAsync();
        int Save();
        Crime AddCrime(Crime crime);
        Task<Crime> AddCrimeAsync(Crime crime);
        void Update(Crime crime);
        Task UpdateAsync(Crime crime);
        void DeleteCrime(Crime crime);
        Crime GetCrime(string criminalNo);
        Task<Crime> GetCrimeAsync(string criminalNo, string status = "");
        Task<Crime> GetCrimeAsync(Expression<Func<Crime, bool>> expression);
        List<Crime> GetCrimes(string criminalNo);
        List<Crime> GetCrimes(Expression<Func<Crime, bool>> expression);
        Task<List<Crime>> GetCrimesAsync(string criminalNo);
        Task<List<Crime>> GetCrimesAsync(Expression<Func<Crime, bool>> expression);



    }
}
