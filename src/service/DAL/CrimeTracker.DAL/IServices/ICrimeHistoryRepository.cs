﻿using CrimeTracker.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.IServices
{
    public interface ICrimeHistoryRepository
    {
        void UpdateCriminalHistory(CriminalHistory criminal);
        void Dispose();
        CriminalHistory AddCriminalHistory(CriminalHistory criminal);
        Task<CriminalHistory> AddCriminalHistoryAsync(CriminalHistory criminal);
        void DeleteCriminalHistory(CriminalHistory criminal);
        CriminalHistory GetCriminalHistory(string criminalNo);
        Task<CriminalHistory> GetCriminalHistoryAsync(string criminalNo);
        List<CriminalHistory> GetCriminalHistories(string criminalNo);
        List<CriminalHistory> GetCriminalHistories(Expression<Func<CriminalHistory, bool>> expression);
        Task<List<CriminalHistory>> GetCriminalHistoriesAsync(string criminalNo);
        Task<List<CriminalHistory>> GetCriminalHistoriesAsync(Expression<Func<CriminalHistory, bool>> expression);
        Task<int> SaveAsync();
        int Save();
    }
}
