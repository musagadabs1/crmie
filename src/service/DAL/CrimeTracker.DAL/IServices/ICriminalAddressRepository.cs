﻿using CrimeTracker.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.IServices
{
    public interface ICriminalAddressRepository
    {
        void Dispose();
        Task<int> SaveAsync();
        int Save();
        void Update(CriminalAddress criminal);
        Task UpdateAsync(CriminalAddress criminal);
        CriminalAddress AddCriminalAddress(CriminalAddress criminalAddress);
        Task<CriminalAddress> AddCriminalAddressAsync(CriminalAddress criminalAddress);
        void DeleteCriminalAddress(CriminalAddress criminalAddress);
        CriminalAddress GetCriminalAddress(string criminalNo);
        Task<CriminalAddress> GetCriminalAddressAsync(Expression<Func<CriminalAddress, bool>> expression);
        Task<CriminalAddress> GetCriminalAddressAsync(string criminalNo,string status="");
        List<CriminalAddress> GetCriminalAddresses();
        List<CriminalAddress> GetCriminalAddresses(Expression<Func<CriminalAddress, bool>> expression);
        Task<List<CriminalAddress>> GetCriminalAddressesAsync();
        Task<List<CriminalAddress>> GetCriminalAddressesAsync(string criminalNo);
        Task<List<CriminalAddress>> GetCriminalAddressesAsync(Expression<Func<CriminalAddress,bool>> expression);
    }
}
