﻿using CrimeTracker.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.IServices
{
    public interface IUserRepository
    {
        User Authenticate(string username, string password);
        Task<User> CreateUser(User user, string password, string rolename);
        Task<User> UpdateUser(int userid, string rolename);
        Task<UserRole> AssignRole(string username, string rolename);
        List<UserRole> GetRoles();
        UserRole GetRoleByRoleName(string roleName);
        bool IsInRole(string username, string rolename);
        List<User> UsersInRole(string rolename);
        void ResetPassword(int userid);

        Task<string> Changepassowrd(string userid, string password);
        void UpdateUser(User user);
        void Dispose();
        User AddUser(User user);
        Task<User> AddUserAsync(User user);
        void DeleteUser(User user);
        User GetUser(string userId);
        Task<User> GetUserAsync(string userId);
        User GetUser(int userRoleId);
        Task<User> GetUserAsync(int userRoleId);
        List<User> GetUsers();
        List<User> GetUsers(Expression<Func<User, bool>> expression);
        Task<List<User>> GetUsersAsync();
        Task<List<User>> GetUsersAsync(Expression<Func<User, bool>> expression);
        Task<int> SaveAsync();
        int Save();
    }
}
