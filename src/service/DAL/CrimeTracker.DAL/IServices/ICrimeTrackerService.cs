﻿using System.Threading.Tasks;

namespace CrimeTracker.DAL.IServices
{
    public interface ICrimeTrackerService
    {
        //DbRawSqlQuery<T> SQLQuery<T>(string sql, params object[] parameters);
        ICrimeRepository Crime { get; }
        ICriminalRepository Criminal { get; }
        ICriminalAddressRepository CriminalAddress { get; }
        //ICriminalImageRepository CriminalImage { get; }
        ICrimeHistoryRepository CrimeHistory { get; }
        IUserRepository User { get; }
        IUserRoleRepository UserRole { get; }
        int Complete();
        Task<int> CompleteAsync();

    }
}
