﻿using CrimeTracker.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.IServices
{
    public interface IUserRoleRepository
    {
        void UpdateUserRole(UserRole userRole);
        void Dispose();
        UserRole AddUserRole(UserRole userRole);
        Task<UserRole> AddUserRoleAsync(UserRole userRole);
        void DeleteUser(UserRole userRole);
        UserRole GetUserRole(string userId);
        Task<UserRole> GetUserRoleAsync(string userId);
        List<UserRole> GetUserRoles();
        List<UserRole> GetUserRoles(Expression<Func<UserRole, bool>> expression);
        Task<List<UserRole>> GetUserRolesAsync();
        Task<List<UserRole>> GetUserRolesAsync(Expression<Func<UserRole, bool>> expression);
        Task<int> SaveAsync();
        int Save();
    }
}
