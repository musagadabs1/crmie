﻿using CrimeTracker.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CrimeTracker.DAL.IServices
{
    public interface ICriminalRepository
    {
        void Update(Criminal criminal);
        Task UpdateAsync(Criminal criminal);
        void Dispose();
        Task<int> SaveAsync();
        int Save();
        Criminal AddCriminal(Criminal criminal);
        Task<Criminal> AddCriminalAsync(Criminal criminal);
        void DeleteCriminal(Criminal criminal);
        Criminal GetCriminal(string id);
        Criminal GetCriminal(Expression<Func<Criminal, bool>> expression);
        Task<Criminal> GetCriminalAsync(string id);
        List<Criminal> GetCriminals();
        List<Criminal> GetCriminals(Expression<Func<Criminal, bool>> expression);
        Task<List<Criminal>> GetCriminalsAsync();
        Task<List<Criminal>> GetCriminalsAsync(Expression<Func<Criminal, bool>> expression);
    }
}
